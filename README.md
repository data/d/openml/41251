# OpenML dataset: flight-delay-usa-dec-2017

https://www.openml.org/d/41251

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Predicting US flight delay in December 2017 based on airline on-time performance data provided by the Bureau of Transportation Statistics (BTS) [https://www.transtats.bts.gov/Tables.asp?DB_ID=120]. Data was downloaded on 03.11.2018. For a description of all variables included in this dataset (many more are available from the BTS website) checkout [https://www.transtats.bts.gov/Fields.asp]. All flights which got canceled or diverted were removed. The dataset includes the variables 'FL_DATE' and 'CRS_DEP_TIME' (ignored by default) which can be used to construct additional time features.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41251) of an [OpenML dataset](https://www.openml.org/d/41251). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41251/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41251/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41251/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

